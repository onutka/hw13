<?php
/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 27.11.2017
 * Time: 23:57
 */
try{
    $pdoDB = new PDO('mysql:host=localhost;dbname=mydb','admin','admin');
    $pdoDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdoDB->exec("SET NAMES 'utf8'");
}catch(PDOException $e){

    die('Нет соединения с базой данных!<br>'.$e->getMessage());

}
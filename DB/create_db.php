<?php
/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 28.11.2017
 * Time: 0:02
 */

require_once 'connect_db.php';
try{
    $sqlQuery = '
		CREATE TABLE portfolio(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			year VARCHAR (255),
			description TEXT,
			url VARCHAR(255)
		) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
	';
    $pdoDB->exec($sqlQuery);
}catch(PDOException $e){
    die('Could not create table portfolio !<br>'.$e->getMessage());}

  try{$sqlQuery = '
		CREATE TABLE articles(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title VARCHAR (255),
			text TEXT
		) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
	';
      $pdoDB->exec($sqlQuery);
} catch(PDOException $e){
    die('Could not create table articles !<br>'.$e->getMessage());
}


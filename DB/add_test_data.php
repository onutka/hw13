<?php
/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 28.11.2017
 * Time: 0:15
 */

require_once 'connect_db.php';

try{ $sql = 'INSERT INTO portfolio SET
				year = "2015",
				url = "https://dp.informator.ua/",
				description = "Информационный портал новости Днепра";    
';
    $pdoDB->exec($sql);
    $sql = 'INSERT INTO portfolio SET
				year = "2016",
				url = "https://www.panama.ua",
				description = "Магазин игрушек и детских товаров";    
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO portfolio SET
				year = "2016",
				url = "https://kloomba.com/",
				description = "Объявления о продаже детской одежды и обуви, колясок, игрушек, женских и мужских вещей, товаров для дома и семьи.";    
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO portfolio SET
				year = "2017",
				url = "https://kloomba.com/",
				description = "Объявления о продаже со всей Украны.";    
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO portfolio SET
				year = "2017",
				url = "https://www.gadget.ua",
				description = "Интернет магазин техники Gadget.ua";    
';
    $pdoDB->exec($sql);

}catch(PDOException $e){
    die('could not add new work!'.$e->getMessage());
}

try{
    $sql = 'INSERT INTO articles SET
				title = "Как организовать работу интернет-магазина",
				text = "Практические советы и рекомендации о том, как наладить работу интернет-магазина, выборе поставщиков и особенности работы с перевозчиками";
                
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO articles SET
				title = "Тренды SEO в 2018",
				text = "Как продвигать сайт в 2018 году? Какие методы все еще актуальны? Как не попасть под фильтр Google?";
                
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO articles SET
				title = "Как дизайн интернет-магазина влияет на средний чек?",
				text = "В данной статье рассмотрим основные характеристики дизайна, которые влияют на средний чек в Вашем магазиине и почему важно обновлять дизайн.";
                
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO articles SET
				title = "Где брать идеи для блога?",
				text = "Эта статья будет полезна тем, кто решил запустить собственный блог. Коротко о том, где брать новые идеи для материалов и сделать информацию интересной и доступной.";
                
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO articles SET
				title = "Какой движок выбрать для сайта? ",
				text = "Обзор и сравнение самых популярных движков, их достоинства и недостатки.";
                
';
    $pdoDB->exec($sql);

    $sql = 'INSERT INTO articles SET
				title = "Что такое CTA, или Как сформировать эффективный призыв к действию",
				text = "В широком смысле под призывом к действию (call to action, CTA) подразумевают все элементы, которые побуждают посетителя приобрести товар или услугу, хотя чаще всего речь идет о кнопках заказа. ";
';
    $pdoDB->exec($sql);
}catch(PDOException $e){
    die('could not add new article'.$e->getMessage());
}

echo 'vse ok!';
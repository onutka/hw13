<?php
/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 14.01.2018
 * Time: 11:34
 */

class Controller_Portfolio extends Controller
{    /*public $model;
     public $itemPort;*/
    function __construct()
    {   parent::__construct();
        $this->model = new Model_Portfolio();
        $this->view = new View();

    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('portfolio', $data);
    }

    function action_show($id){
        $data = $this->model->get_portfolio($id);
        $this->view->generate('portfolio_item', $data);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 14.01.2018
 * Time: 11:35
 */

class Controller_Articles extends Controller
{
    function __construct()
    {   parent::__construct();
        $this->model = new Model_Articles();
        $this->view = new View();

    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('articles', $data);
    }

    function action_show($id){
        $data = $this->model->get_article($id);
        $this->view->generate('articles_item', $data);
    }
}